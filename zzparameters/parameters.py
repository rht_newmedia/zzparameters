#!/usr/bin/env python
#
#  Copyright (c) 2015 Red Hat, Inc.  <bowe@redhat.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
"""
"""
__author__  = 'Bowe Strickland <bowe@redhat.com>'
__docformat__ = 'restructuredtext'

import logging; log = logging.getLogger(__name__)
DEBUG=log.debug; INFO=log.info; WARN=log.warning; ERROR=log.error
#logging.basicConfig(level=logging.INFO)

import os

from zzutils import read_yaml_file
from zca import zca_manager as zca
from .interfaces import IParameters

@zca.implementer(IParameters)
class Parameters(dict):

    def __init__(self, *args, **kw):

        super(Parameters, self).__init__(*args, **kw)

    def _merge_dicts(self, dst, src):

        for k,v in src.items():

            if isinstance(v, dict):
                self._merge_dicts(dst.setdefault(k, {}), v)
            else:
                dst[k] = v

        return dst

    def _dump_dict(self, target=None, collector=None, depth=1):
        
        target = self if target is None else target
        collector = [] if collector is None else collector
        pad = "    "*depth

        for k in sorted(target):

            v = target[k]
            if isinstance(v, dict):
                if v:
                    collector.append("%s%s:" % (pad, k))
                    self._dump_dict(v, collector, depth+1)
            else:
                collector.append("%s%s: %s" % (pad, k, str(v)))

        return collector
        
    def dump_config(self):

        aa = ["Parameters:", ]
        aa.extend(self._dump_dict())
        return os.linesep.join(aa)

    def set_parameter(self, name, value, base=None):

        base = self if base is None else base

        if "." not in name:
            base[name] = value
            return

        head, rest = name.split(".", 1)
        next_base = base.setdefault(head, {})
        return self.set_parameter(rest, value, next_base)

    def get_parameter(self, name, default=None, base=None):

        base = self if base is None else base

        if "." not in name:
            return base.get(name, default)

        head, rest = name.split(".", 1)
        next_base = base.get(head, {})
        return self.get_parameter(rest, default, next_base)

    def load_data(self, data):
        self._merge_dicts(self, data)

    def load_param_file(self, filename, paths=None, must_exist=False):

        paths = paths or []

        def _find_param_file(filename, paths):

            if os.path.exists(filename):
                return filename

            for p in paths:
                fpath = os.path.join(p, filename)
                if os.path.exists(fpath):
                    return fpath    # pragma: nocover

            if must_exist:
                msg = "param file %s does not exist using %s"
                raise ValueError(msg % (filename, ":".join(paths)))

        fname = _find_param_file(filename, paths)
        if not fname:
            DEBUG("skipping nonexistant parameter file %s" % filename)
            return

        INFO("reading parameters from %s" % filename)
        self.load_data(read_yaml_file(filename))

    def load_param_files(self, filenames, paths=None, must_exist=False):

        for f in filenames:
            self.load_param_file(f, paths, must_exist)


# vi: ts=4 expandtab


