#!/usr/bin/env python
#
#  Copyright (c) 2015 Red Hat, Inc.  <bowe@redhat.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
"""
"""
__author__  = 'Bowe Strickland <bowe@redhat.com>'
__docformat__ = 'restructuredtext'

import logging; log = logging.getLogger(__name__)
DEBUG=log.debug; INFO=log.info; WARN=log.warning; ERROR=log.error

from zope.interface import Interface, Attribute

class IParameters(Interface):

    """Paramaters derives from dict superclass"""

    def set_parameter(name, value):

        """ set the parameter.  Names can contain ".", which will
            be interpretred as references to nested dictionaries.
        """

    def get_parameter(name, default=None):

        """ get the parameter.  Names can contain ".", which will
            be interpretred as references to nested dictionaries.

            If the parameter doesn't exist, default is returned 
            instead.
        """
        
    def dump_config():

        """ return a text buffer of dumped paramaters. """

    def load_data(data):

        """ merges a dictionary of data """

    def load_param_file(filename, paths=None, must_exist=False):

        """ merge a yaml formatted filenames """

    def load_param_files(filenames, paths=None, must_exist=False):

        """ merge an array of yaml formatted param filenames """

# vi: ts=4 expandtab


