#!/usr/bin/env python
#
#  Copyright (c) 2015 Red Hat, Inc.  <bowe@redhat.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
"""
"""
__author__  = 'Bowe Strickland <bowe@redhat.com>'
__docformat__ = 'restructuredtext'

import logging; log = logging.getLogger(__name__)
DEBUG=log.debug; INFO=log.info; WARN=log.warning; ERROR=log.error
#logging.basicConfig(level=logging.INFO)

import os
import unittest
from tempfile import NamedTemporaryFile

import yaml

from nose.tools import *

from ..parameters import Parameters

param_dict_yaml = """
foo: bar
biz: 
    bizfoo: baz
    bizbaz: 3
""".strip()

param_dict = yaml.load(param_dict_yaml)

param_list = """
- foo
- bar
""".strip()


class TestParams(unittest.TestCase):

    def test_paramfile(self):

        with NamedTemporaryFile() as f:

            f.write(param_dict_yaml)
            f.flush()

            params = Parameters()
            params.load_param_file(f.name)
            params.dump_config()

    @raises(ValueError)
    def test_noexist_paramfile(self):

        params = Parameters()
        params.load_param_files(["fasfsadsf"], must_exist=True)

    def test_ok_noexist_paramfile(self):

        params = Parameters()
        params.load_param_files(["fasfsadsf"], must_exist=False)

    def test_paramfiles_path(self):

        params = Parameters()
        params.load_param_files(
            ["fasfsadsf"], 
            paths=["/tmp", "/var/tmp",],
            must_exist=False
        )

    def test_parameter(self):

        params = Parameters()
        params.set_parameter("foo.bar.biz", 42)
        eq_(params['foo']['bar']['biz'], 42)

    def test_get_parameter(self):

        params = Parameters()
        params.load_data(param_dict)

        v = params.get_parameter("biz.bizbaz")
        eq_(v, 3)

# vi: ts=4 expandtab


